// 利用 Symbol 实现私有变量和私有方法，外界不可访问（参考链接2）
const 魔断配置 = {
    setUrl: Symbol('setUrl'),
    set_switch: Symbol('set_switch'),
    path: Symbol('path'),
    x5cache: Symbol('x5cache'),
};

var tools = {
    MD5: function(data) {
        eval(getCryptoJS());
        return CryptoJS.MD5(data).toString(CryptoJS.enc.Hex);
    },
    AES: function(text, key, iv, isEncrypt) {
        eval(getCryptoJS());
        var key = CryptoJS.enc.Utf8.parse(key);
        var iv = CryptoJS.enc.Utf8.parse(iv);
        if (isEncrypt) {
            return CryptoJS.AES.encrypt(text, key, {
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }).toString();
        };
        return CryptoJS.AES.decrypt(text, key, {
            iv: iv,
            padding: CryptoJS.pad.Pkcs7
        }).toString(CryptoJS.enc.Utf8);
    },
    //ascii
    nextCharacter: function(asciiValue, k) {
        var s = asciiValue;
        return String.fromCharCode(s + k);
    },
    //凯撒
    caesarCipher: function(stringValue, k) {
        var newString = "";
        for (var i = 0; i < stringValue.length; i++) {
            newString += this.nextCharacter(stringValue[i].charCodeAt(), k);
        }
        return newString;
    },
    nowDate: function() {
        var date1 = new Date();
        var dateStr = "";
        if (date1) {
            dateStr = date1.getFullYear();
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month < 10) {
                dateStr += "-0" + month;
            } else {
                dateStr += "-" + month;
            }
            if (day < 10) {
                dateStr += "-0" + day;
            } else {
                dateStr += "-" + day;
            }
        }
        return dateStr;
    },
    toJSON: function(json) {
        return JSON.stringify(json, (key, value) => {
            if (typeof value == 'function') {
                return value.toString();
            } else {
                return value;
            }
        }, 4);
    },
    toVNum: function(a) {
        var a = a.toString();
        var c = a.split('.');
        var num_place = ["", "0", "00", "000", "0000"],
            r = num_place.reverse();
        for (var i = 0; i < c.length; i++) {
            var len = c[i].length;
            c[i] = r[len] + c[i];
        }
        var res = c.join('');
        return res;
    },
    atob:function (str){
        require('https://cdn.jsdelivr.net/npm/js-base64@3.7.2/base64.min.js');
        return Base64.atob(str);
    },
    btoa:function (str){
        require('https://cdn.jsdelivr.net/npm/js-base64@3.7.2/base64.min.js');
        return Base64.btoa(str);
    },
    江湖:function (config_url, tem, token) {
        require('https://cdn.jsdelivr.net/npm/js-base64@3.7.2/base64.min.js');
        var atob=Base64.atob;
        var btoa=Base64.btoa;
        var config_a = tem || "ffsirllq";
        var video = '';
        var tem = [];
        var config_arry = [];
        var config_b = config_a.length;
        if (token == 0x1) {
            var config_url = atob(config_url);
        } else {
            var config_url = encodeURIComponent(config_url);
        }
        var config_c = config_url.length;
        for (i = 0x0; i < 0x100; i++) {
            tem[i] = config_a[i % config_b].charCodeAt();
            config_arry[i] = i;
        }
        for (j = i = 0x0; i < 0x100; i++) {
            j = (j + config_arry[i] + tem[i]) % 0x100;
            tmp = config_arry[i];
            config_arry[i] = config_arry[j];
            config_arry[j] = tmp;
        }
        for (a = j = i = 0x0; i < config_c; i++) {
            a = (a + 0x1) % 0x100;
            j = (j + config_arry[a]) % 0x100;
            tmp = config_arry[a];
            config_arry[a] = config_arry[j];
            config_arry[j] = tmp;
            k = config_arry[(config_arry[a] + config_arry[j]) % 0x100];
            video += String.fromCharCode(config_url[i].charCodeAt() ^ k);
        }
        log(decodeURIComponent(video));
        if (token == 0x1) {
            return decodeURIComponent(video);
        } else {
            return btoa(video);
        }
    }
};

function MyField(path) {
    // 'use strict';
    // ...
    // 魔断配置路径
    this[魔断配置.path] = path || "hiker://files/cache/MyParseSet.json";
    this[魔断配置.x5cache] = 'hiker://files/cache/Parse_Dn.html';
    this.VARMAP = {//myVar局部变量
        AUTO: "魔断.auto",
        TIMEOUT: "魔断.timeout",
    }
    //断念插件调度设置
    let setUrl = "hiker://page/Route?rule=MyFieldᴰⁿ&type=设置#noRecordHistory#";
    this[魔断配置.setUrl] = setUrl;
    this[魔断配置.set_switch] = setUrl
}

Object.assign(MyField.prototype, {
    // Override 构造方法，相当于 function.prototype.constructor = (...) => {...}，new function() 的时候会自动执行
    constructor: MyField,
    ParseS:{
        RX解析专用: function(vipUrl) {
            eval(getCryptoJS());
            var parse = "https://80k.tv/player/analysis.php?v=";
            var html = fetch(parse + vipUrl, {
                headers: {
                    "Referer": "https://80k.tv/",
                    "User-Agent": PC_UA
                }
            });
            var PAR = {
                secret(word, code, isDecrypt) {
                    code = CryptoJS.MD5(code).toString();
                    var iv = CryptoJS.enc.Utf8.parse(code.substring(0, 16));
                    var key = CryptoJS.enc.Utf8.parse(code.substring(16));
                    if (isDecrypt) {
                        return CryptoJS.AES.decrypt(word, key, {
                            iv: iv,
                            padding: CryptoJS.pad.Pkcs7
                        }).toString(CryptoJS.enc.Utf8);
                    }
                    return CryptoJS.AES.encrypt(word, key, {
                        iv: iv,
                        mode: CryptoJS.mode.CBC,
                        padding: CryptoJS.pad.Pkcs7
                    }).toString();
                },
                compare(key) {
                    return function(a, b) {
                        return a[key] - b[key];
                    };
                }
            };
            var config = {
                url: pdfh(html, "body&&script&&Html").match(/"url": "(.*)"/)[1]
            };
            var _pr = pdfh(html, "meta[name=\"viewport\"]&&id").replace("vod_", "");
            var _pu = pdfh(html, "meta[charset=\"UTF-8\"]&&id").replace("vod_", "");
            var _puArr = [];
            var _newArr = [];
            var _code = "";
            for (var i = 0; i < _pu.length; i++) {
                _puArr.push({
                    id: _pu[i],
                    text: _pr[i]
                });
            }
            _newArr = _puArr.sort(PAR.compare("id"));
            for (var i = 0; i < _newArr.length; i++) {
                _code += _newArr[i].text;
            }
            config.url = PAR.secret(config.url, _code, true);
            return config.url;
        },
        '猫':function (jxurl, ref, key) {
            try {
                var getVideoInfo = function (text) {
                    return tools.AES(text, key, iv);
                };
                let headers = {headers: {"Referer": ref||''}};
                var html = ref?request(jxurl, headers):request(jxurl);
                if (/&btwaf=/.test(html)) {
                    html = request(jxurl + "&btwaf" + html.match(/&btwaf(.*?)"/)[1], {headers: {"Referer": ref}});
                }
                var iv = html.split("_token = \"")[1].split("\"")[0];
                eval(html.match(/var config = {[\s\S]*?}/)[0] + "");
                if (!/^http/.test(config.url)) {
                    config.url = decodeURIComponent(tools.AES(config.url, "dvyYRQlnPRCMdQSe", iv));
                }
                return config.url;
            }
            catch (e) {
                return "";
            }
        },
    },
    魔断:function (){
        return this.ParseS
    },
    获取源码: function(url,ua,referer){//传url,ua和refer
        ua = ua||PC_UA;
        let headers = {
            'User-Agent': ua
        };
        if(typeof(referer)!=='undefined'&&referer.length>4){
            headers.Referer = referer
        }
        let html = fetch(url, {
            headers: headers
        });
        return html
    },
    xfyun: function(vipUrl) {
        let html=this.获取源码('https://jx.ak1080.me/?url='+vipUrl,MOBILE_UA,'https://bwl87.com/play/');
        let url=html.match(/"url":(.*?)"(.*?)",/)[2];
        return url + ';{User-Agent@Mozilla/5.0}';
    },
    xin:function (vipUrl){
        let html=this.获取源码('https://haha.90mm.me/play.php?url='+vipUrl,MOBILE_UA,'https://bwl87.com/play/');
        let url=html.match(/"url":(.*?)"(.*?)",/)[2];
        return cacheM3u8(url,{
            headers: {"User-Agent":"Mozilla/5.0","Referer": 'https://bwl87.com/play/'}
        })
    },
    duoduozy:function (vipUrl){
        let html = request("https://player.duoduozy.com/ddplay/?url=" + vipUrl,
            {headers:{"User-Agent":"Mozilla/5.0",'Referer':'https://www.tgys.tv/'}});
        return html.match(/urls = "(.*?)"/)[1]+';{User-Agent@Mozilla/5.0}';
    },
    renrenmi:function (vipUrl){
        // let rrjx = 'https://cache1.jhdyw.vip:8091/rrmi.php?url=';
        // let rrjx = 'https://jx.18mv.club/rrm/?url=';
        // let url=JSON.parse(fetch(rrjx + vipUrl)).url;
        // return cacheM3u8(url)
        return this.json('http://jf.jisutuku.top/api/?key=TVDrWfMwbn1IUtLLWY&url='+vipUrl)
    },
    json:function (playUrl,timeout){
        timeout = timeout||5000;
        try {
            let url = JSON.parse(request(playUrl,{timeout:timeout})).url;
            if(!(/User-Agent|Referer@/.test(url))) {
                if (/lecloud\.com|bilivideo\.com/.test(url)) {
                    url += ";{Referer@https://www.bilibili.com/&&User-Agent@Mozilla/5.0}";
                } else if (/ixigua\.com/.test(url)) {
                    url += "#.mp4;{Referer@https://www.ixigua.com/&&User-Agent@Mozilla/5.0}";
                } else if (/mgtv\.com/.test(url)) {
                    url += ";{User-Agent@Mozilla/5.0}";
                } else if (/ptwo\.wkfile\.com/.test(url) && /url=/.test(url)) {
                    url = url.split("url=")[1] + ";{Referer@https://fantuan.tv}"
                } else {
                    url = cacheM3u8(url)
                }
            }
            return url
        }catch (e) {
            log('魔断.js:错误的json接口:'+playUrl+',error:'+e.message);
            return playUrl
        }
    },
    jsonCache:function (playUrl,params,cache){
        params = params||{};
        cache = cache || false;
        try {
            let url=JSON.parse(fetch(playUrl,params)).url;
            if(cache){
                url = cacheM3u8(url)
            }
            return url
        }catch (e) {
            log('发生错误,开始跳网页:'+e.message);
            return playUrl
        }
    },
    DP:function (vipUrl){
        eval("var config =" + fetch(this.path));
        eval(fetch(config.cj));
        return aytmParse(vipUrl)
    },
    PPTV:function (vipUrl){
        // return this.jsonCache('https://apis.jxcxin.cn/api/jx?url='+vipUrl);
        let url = this.ParseS.猫("https://17.txtest.qzbeyond.cn/dmpl/?url=" + vipUrl, false, "A52EAC0C2B508579");
        return url;
    },
    咪咕:function (vipUrl){
        return this.jsonCache('http://jx.nokia.press/api/migu?key=daozhangyyds&url='+vipUrl)
    },
    RF:function (vipUrl){
        //瑞丰资源 jx:https://vip.ruifenglb.com:4433/?url=
        // https://1.ruifenglb.com/player/?url=
        return vipUrl+';{User-Agent@Mozilla/5.0&&Referer@https://1.ruifenglb.com}'
    },
    tools:tools,
    RR:function(v){return this.renrenmi(v)},
    XF:function(v){return this.xfyun(v)},
    DD:function(v){return this.duoduozy(v)},
    XIN:function(v){return this.xin(v)}
});
// 示例 魔断[魔断配置.setUrl]
$.exports = new MyField();
$.exports
