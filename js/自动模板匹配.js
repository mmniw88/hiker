var 一级定位模板 = [];
var 二级定位模板 = [];
一级定位模板.push({
    名称:'阿房影视',
    自动初始分类:true,
    初始分类链接定位:'.myui-header__menu&&li,1&&a&&href',
    初始分类链接处理:(u)=>{
        return u.replace(/\/type\/(.*?)\//,'/show/$1--------1---');
    },
    一级解析:'.myui-vodlist&&li;.lazyload&&title;.lazyload&&data-original;.lazyload&&Text;.lazyload&&href',
    动态分类列表:[{
        一级分类: 'body&&ul.myui-header__menu',
        子分类: 'body&&li.hidden-sm:gt(0):lt(5)',
    }, {
            一级分类: 'body&&.myui-screen__list',
            子分类: 'ul&&li:has(a[href]):not(:matches(^$))',
        }
    ],
    一级处理:{},
    关闭折叠:false,
    设置倒放:false,
    是否有二级:true,
    免责:false,
});
二级定位模板.push({
    二级解析:{
        title:'h1.title&&Text;.data,1&&Text;#rating&&.branch&&Text',
        img:'.myui-content__thumb&&a&&img&&data-original',
        url:'.myui-content__thumb&&a&&href',
        desc:'.data,2&&Text;.data,3&&Text',
        content:'#desc&&span.data&&Text',
        tabs:'ul.nav-tabs&&li',
        lists:'.tab-content&&#id&&li',
        tab_id:'a&&href'
    },
    名称:'阿房影视',
    免嗅:false,
    需要魔断:false,
});

var 自动一级 = function (){
    evalPrivateJS('4ZFai6WdmDyH6oz/nhtVROAIx1c0dRGZtfmf6PyWCr6dHnPVIOLddKI4gXInyEoixmC+cyqM8V/plDjccHPKYdMRRu3DLjeWtkw/W5GbPWQVKdbbEohqdTqYxEM0KWdh');
    MY_URL=MY_URL.split("##")[1].split('#')[0];
    require(config.模板);
    var page = MY_PAGE;
    var 匹配成功 = false;
    for(let i in 一级定位模板){
        let it = 一级定位模板[i];
        try {
            if(it.自动初始分类) {
                初始分类页(it.初始分类链接定位, it.初始分类链接处理);
            }
            let parStr = it.一级解析;
            true_url = 获取正确链接();
            var html = 获取源码(true_url);
            let cates = 打造动态分类(it.动态分类列表,{源码:html,关闭折叠:it.关闭折叠});
            设置(cates, it.设置倒放);
            一级(parStr, it.是否有二级, cates, it.免责, html);
            if(parseInt(page)===1){
                log('一级匹配成功,第'+i+'个模板:【'+it.名称+'】');
            }
            匹配成功 = true;
            break;
        }catch (e) {
            log('一级模板【'+it.名称+'】匹配失败,尝试下一个模板\n'+e.message)
        }
    }
    if(!匹配成功){
        throw new Error("全部已有一级模板匹配失败,请扩充模板库");
    }
}

var 自动二级 = function (){
    require(config.模板);
    var 动态最新章节=true;
    var 倒序=false;
    var lazy = $('').lazyRule((解析)=>{
        var 全局免嗅 = function (input){
            try {
                let ret=fetch(input).match(/var player_(.*?)=(.*?)</)[2];
                let url = JSON.parse(ret).url;
                if(/\.m3u8|\.mp4/.test(url)){
                    return 解析.是否视频(url)
                }else if(!/http/.test(url)&&!/\//.test(url)){
                    try {
                        url = unescape(base64Decode(url));
                        return 解析.纯通免(url)
                    }catch (e) {
                        return 解析.纯通免(input)
                    }
                }
            }catch (e) {
                return 解析.纯通免(input)
            }
        };
        return 全局免嗅(input)
    },解析);
    var 匹配成功 = false;
    for(let i in 二级定位模板){
        let it = 二级定位模板[i];
        let parse=it.二级解析;
        try {
            二级(parse,lazy,it.需要魔断,true);
            log('二级匹配成功,第'+i+'个模板:【'+it.名称+'】');
            匹配成功 = true;
            break;
        }catch (e) {
            log('二级模板【'+it.名称+'】匹配失败,尝试下一个模板\n'+e.message)
        }
    }
    if(!匹配成功){
        throw new Error("全部已有二级模板匹配失败,请扩充模板库");
    }
}
